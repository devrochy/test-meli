### Operacion quasar

Api que permite determinar la ubicación de una nave en el espacio utilizando el método matemático Trilateración y obtener un mensaje de auxilio. La Api permite recibir la información de los satellites de manera independiente para después determinar la posición y obtener el mensaje de auxilio.

## Documentacion API

Se adjunta [documentacion](https://app.swaggerhub.com/apis/dev-rochy/meli-quasar/1.0.0) del Api

### Set de Pruebas

Se adjunta un conjunto de pruebas del servicio en la herramienta [Postman](https://www.postman.com/winter-satellite-98616/workspace/e2f6466d-c35e-4848-b432-c5c67d8480f2/request/11227912-9a2d9fb6-fd24-4081-92f1-c1d2c30f91ca)

## Diagrama de clase

Se adjunta imagen con la arquitectura implementada para relacionar los objetos que se crearon dentro de la arquitectura

![Image text](/images/diagram-meli.png)
