package com.quasar.meli.models;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Clase que contiene la informacion de los satelites 
 * 
 * @author Robinson Aguilar Ramirez, devrochy@gmail.com
 */

@Data
@NoArgsConstructor
public class SatelliteWrapper {
	
	private static final int QUANTITY_SATELLITE = 3;

	@Autowired
	private Environment environment;

	private List<Satellite> satellites = new ArrayList<>();

	public SatelliteWrapper(List<Satellite> satellites) {
		this.satellites = satellites;
	}
	
	/*
	 * obtiene una lista con la informacion de las distancia de los satelites
	 */
	public double[] getDistances() {
		double[] distances = new double[QUANTITY_SATELLITE];
		for (int i = 0; i < QUANTITY_SATELLITE; i++) {
			distances[i] = satellites.get(i).getDistance();
		}
		return distances;
	}

	/*
	 * obtiene una matriz con la informacion de la posicion de los satelites
	 */
	public double[][] getPositions() {
		double[][] positions = new double[QUANTITY_SATELLITE][];
		String[] points;
		for (int i = 0; i < QUANTITY_SATELLITE; i++) {
			if (satellites.get(i).getCoordinate() != null) {
				points = satellites.get(i).getCoordinate().toString().split(",");
				positions[i] = Arrays.stream(points).map(Double::valueOf).mapToDouble(Double::doubleValue).toArray();
			}
		}
		return positions;
	}
	
	/**
	 * Validar que las posiciones de los satelites esten asignadas correctamente
	 * @param pointsList
	 */
	public boolean validatePositions() {
		double[][] positions = this.getPositions();
		for (int i = 0; i < positions.length; i++) {
			if (positions[i] == null) {
				return false;
			}
		}
		return true;
	}

	/*
	 * Asigna la posicion en coordenadas para cada uno de los satelites
	 */
	public void setPositions(double[][] pointsList) {
		Coordinate coordinate;
		for (int i = 0; i < pointsList.length; i++) {
			if (Objects.nonNull(pointsList[i])) {
				coordinate = new Coordinate(pointsList[i][0], pointsList[i][1]);
				satellites.get(i).setCoordinate(coordinate);
			}
		}
	}

	/*
	 * Obtiene lista con el arreglo de cada mensaje emitido por el satelite
	 */
	public List<List<String>> getMessages() {
		List<List<String>> messages = new ArrayList<List<String>>();
		for (Satellite s : satellites) {
			messages.add(s.getMessage());
		}
		return messages;
	}
}
