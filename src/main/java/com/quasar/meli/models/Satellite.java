package com.quasar.meli.models;

import java.util.List;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * Model satelite 
 * 
 * @author Robinson Aguilar Ramirez, devrochy@gmail.com
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class Satellite extends Ship {
	private String name;
	private double distance;
	private List<String> message;
	
	@Builder
	private Satellite(Coordinate coordinate, double distance, List<String> message, String name) {
		super(coordinate);
		this.distance = distance;
		this.message = message;
		this.name = name;
	}
	
}
