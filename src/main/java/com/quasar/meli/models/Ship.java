package com.quasar.meli.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Model ship 
 * 
 * @author Robinson Aguilar Ramirez, devrochy@gmail.com
 * @since 1.0
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Ship {
	
	private Coordinate coordinate;

}
