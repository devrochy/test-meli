package com.quasar.meli.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

/**
 * Model coordenadas en un plano XY
 * 
 * @author Robinson Aguilar Ramirez, devrochy@gmail.com
 */
@Data
@Builder
@AllArgsConstructor
public class Coordinate {
	
	private double positionX;
	private double positionY;
	
	@Override
	public String toString() {
		return positionX + "," + positionY;
	}
	
	

}
