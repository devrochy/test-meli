package com.quasar.meli.exceptions;

/**
 * Clase excepcion para el mensaje
 * 
 * @author Robinson Aguilar Ramirez, devrochy@gmail.com
 */
public class MessageException extends Exception {

	private static final long serialVersionUID = -1142538241232583674L;

	public MessageException(String errorMessage) {
		super(errorMessage);
	}
}
