package com.quasar.meli.exceptions;

/**
 * Clase excepcion para la ubicacion
 * 
 * @author Robinson Aguilar Ramirez, devrochy@gmail.com
 */
public class LocationException extends Exception{

	private static final long serialVersionUID = 3006628958111284706L;

	public LocationException(String errorMessage){
        super(errorMessage);
    }
}
