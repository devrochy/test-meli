package com.quasar.meli.utils;

/**
 * Constantes de logs y mensajes de exceptions
 * 
 * @author Robinson Aguilar Ramirez, devrochy@gmail.com
 */
public class MessageUtil {
	
	/**
	 * Message response
	 */
	public static final String MESSAGE_OK = "ok";

	/**
	 * Message log
	 */
	public static final String START_FUNCTION = "Start function.....";
	public static final String FINISH_FUNCTION = "End function.....";

	/**
	 * Message exceptions
	 */
	public static final String MESSAGE_INSUFFICIENT_INFORMATION = "Numero de mensajes insuficientes";
	public static final String MESSAGE_INSUFFICIENT_POSITION = "Numero de posicion o distancias insuficientes";
}
