package com.quasar.meli.controller;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import com.quasar.meli.exceptions.LocationException;
import com.quasar.meli.exceptions.MessageException;
import com.quasar.meli.pojos.TopSecretRequest;
import com.quasar.meli.pojos.TopSecretResponse;
import com.quasar.meli.pojos.TopSecretSplitRequest;
import com.quasar.meli.services.IntelligenceServiceImpl;
import com.quasar.meli.utils.MessageUtil;

import lombok.extern.slf4j.Slf4j;

/**
 * Controlador que comunica los satelites con el servicio de inteligencia
 * 
 * @author Robinson Aguilar Ramirez, devrochy@gmail.com
 */
@Slf4j
@RestController
@RequestMapping("/v1")
public class CommunicationController {

	@Autowired
	private IntelligenceServiceImpl intelligenceService;

	/**
	 * Determina la posicion de la nave que esta enviando el mensaje de auxilio
	 *
	 * @param request, lista de los satelites con el mensaje de auxilio en forma de arreglo y la distancia 
	 * 
	 * @return ubicación de la nave y el mensaje que emite 
	 */
	@PostMapping(value = "/topsecret", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<TopSecretResponse> topSecret(@Valid @RequestBody final TopSecretRequest request) {
		log.trace(MessageUtil.START_FUNCTION);
		try {
			TopSecretResponse topSecretResponse = intelligenceService.getHelpMessage(request);
			log.trace(MessageUtil.FINISH_FUNCTION);
			return new ResponseEntity<>(topSecretResponse, HttpStatus.OK);
		} catch (MessageException e) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage(), e);
		} catch (LocationException e) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage(), e);
		}
	}

	/**
	 * Almacena la información enviada por un satelite
	 *
	 * @param satelliteName, nombre del satelite
	 * @param request, solicitud con el mensaje en forma de arreglo y la distancia del satelite
	 * @param httpServletRequest http para obtener la ip de la solicitud de auxilio
	 * 
	 * @return http Ok confirmando el almacenamiento del llamado de auxilio  
	 */
	@PostMapping(value = "/topsecret_split/{satellite_name}", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<TopSecretResponse> topSecretSplit(
			@PathVariable(value = "satellite_name") @NotBlank final String satelliteName, 
			@Valid @RequestBody final TopSecretSplitRequest request, HttpServletRequest httpServletRequest) {
		log.trace(MessageUtil.START_FUNCTION);
		try {
			TopSecretResponse topSecretResponse = intelligenceService.saveInformation(satelliteName, request, httpServletRequest.getRemoteAddr());
			log.trace(MessageUtil.FINISH_FUNCTION);
			return new ResponseEntity<>(topSecretResponse, HttpStatus.OK);
		} catch (Exception e) {
			return null;
		}

	}

	/**
	 * Recupera la informacion de auxilio de los satelites si es posible
	 *
	 * @param httpServletRequest http para obtener la ip de la solicitud de auxilio
	 * 
	 * @return retorna el mensaje de auxilio o error 404  
	 */
	@GetMapping(value = "/topsecret_split", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<TopSecretResponse> topSecretSplit(HttpServletRequest httpServletRequest)
			throws MessageException, LocationException { // ,
																							// MessageNotDeterminedException,
		log.trace(MessageUtil.START_FUNCTION);
		try {
			TopSecretResponse topSecretResponse = intelligenceService
					.getInformation(httpServletRequest.getRemoteAddr());
			log.trace(MessageUtil.FINISH_FUNCTION);
			return new ResponseEntity<>(topSecretResponse, HttpStatus.OK);
		} catch (MessageException e) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage(), e);
		} catch (LocationException e) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage(), e);
		}

	}

}
