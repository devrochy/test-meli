package com.quasar.meli.pojos;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.quasar.meli.models.Satellite;

/**
 * Pojo con la informacion de los satelites con el mensaje de auxilio
 * 
 * @author Robinson Aguilar Ramirez, devrochy@gmail.com
 */
public class TopSecretRequest {

	@JsonProperty("satellites")
	private List<Satellite> satellites = null;

	@JsonProperty("satellites")
	public List<Satellite> getSatellites() {
		return satellites;
	}

	@JsonProperty("satellites")
	public void setSatellites(List<Satellite> satellites) {
		this.satellites = satellites;
	}
}
