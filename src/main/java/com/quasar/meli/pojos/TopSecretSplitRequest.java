package com.quasar.meli.pojos;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Builder;
import lombok.Data;

/**
 * Pojo con la informacion de auxilio de un solo satelite
 * 
 * @author Robinson Aguilar Ramirez, devrochy@gmail.com
 */
@Data
@Builder
public class TopSecretSplitRequest {

	@JsonProperty("distance")
	private double distance;
	
	@JsonProperty("message")
	private List<String> message;
}
