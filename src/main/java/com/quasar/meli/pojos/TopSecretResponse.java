package com.quasar.meli.pojos;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.quasar.meli.models.Coordinate;

import lombok.Builder;
import lombok.Data;

/**
 * Pojo para responder las peticiones del servicio de inteligencia
 * 
 * @author Robinson Aguilar Ramirez, devrochy@gmail.com
 */
@Data
@Builder
public class TopSecretResponse {

	@JsonProperty("position")
	public Coordinate coordinate;

	@JsonProperty("message")
	public String message;
	
	public TopSecretResponse(Coordinate coordinate, String message) {
		this.coordinate = coordinate;
		this.message = message;
	}
}
