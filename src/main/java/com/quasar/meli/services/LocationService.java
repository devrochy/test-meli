package com.quasar.meli.services;

import org.apache.commons.math3.fitting.leastsquares.LevenbergMarquardtOptimizer;
import org.springframework.stereotype.Service;

import com.lemmingapex.trilateration.NonLinearLeastSquaresSolver;
import com.lemmingapex.trilateration.TrilaterationFunction;

/**
 * Servicio para obtener la ubicacion utilizando la function Trilateration
 * 
 * @author Robinson Aguilar Ramirez, devrochy@gmail.com
 */
@Service
public class LocationService {
	
	/**
	 * Obtiene un punto utilizando la formula trilateration
	 * 
	 * @param positions
	 * @param distances
	 * @return
	 */
	public double [] getLocation(double[][] positions, double [] distances) {
		
		TrilaterationFunction tf = new TrilaterationFunction(positions, distances);
		NonLinearLeastSquaresSolver nSolver = new NonLinearLeastSquaresSolver(tf, new LevenbergMarquardtOptimizer());
		
		return nSolver.solve().getPoint().toArray();
	}

}
