package com.quasar.meli.services;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import com.quasar.meli.exceptions.LocationException;
import com.quasar.meli.exceptions.MessageException;
import com.quasar.meli.models.Coordinate;
import com.quasar.meli.models.Satellite;
import com.quasar.meli.models.SatelliteWrapper;
import com.quasar.meli.pojos.TopSecretRequest;
import com.quasar.meli.pojos.TopSecretResponse;
import com.quasar.meli.pojos.TopSecretSplitRequest;
import com.quasar.meli.utils.MessageUtil;

import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

/**
 * Servicio de inteligencia para obtener, almacenar y recuperar el mensaje de auxilio y la ubicación de la nave
 * 
 * @author Robinson Aguilar Ramirez, devrochy@gmail.com
 */
@Slf4j
@Service
public class IntelligenceServiceImpl implements IntelligenceService {

	@Setter
	private Map<String, TopSecretRequest> cache = new HashMap<>();

	@Autowired
	LocationService locationService;

	@Autowired
	MessageService messageService;

	@Autowired
	private Environment environment;
	
	

	/**
	   * Funcion que reune el llamado de los satelites y recupera el mensaje de auxilio y la ubicación de la nave
	   * 
	   * @param request Lista de los satelites con los mensajes y la distancia
	   * @return mensaje de auxilio y ubicacion de la nave
	   * 
	   * @throws PositionNotDeterminedException if the position cannot be found with the mathematical model
	   * @throws MessageNotDeterminedException if the message cannot be found
	   */
	@Override
	public TopSecretResponse getHelpMessage(final TopSecretRequest request) throws MessageException, LocationException {
		
		log.debug(Thread.currentThread().getStackTrace()[1].getMethodName().concat(": ").concat(MessageUtil.START_FUNCTION));
		
		SatelliteWrapper satelliteWrapper = new SatelliteWrapper(request.getSatellites());
		if (satelliteWrapper.getMessages().size() < 3) {
			throw new MessageException(MessageUtil.MESSAGE_INSUFFICIENT_INFORMATION);
		}
		String message = messageService.getMessage(satelliteWrapper.getMessages());

		//satelliteWrapper.setSatellites(satelliteWrapper.getSatellites().stream().filter(satellite -> Objects.nonNull(satellite.getCoordinate())).collect(Collectors.toList()));
		uploadPositions(satelliteWrapper);
		if (!satelliteWrapper.validatePositions()) {
			throw new LocationException(MessageUtil.MESSAGE_INSUFFICIENT_POSITION);
		}

		double[] points = locationService.getLocation(satelliteWrapper.getPositions(), satelliteWrapper.getDistances());
		Coordinate coordinate = new Coordinate(Math.round(points[0]*100.0)/100.0, Math.round(points[1]*100.0)/100.0);

		log.debug(Thread.currentThread().getStackTrace()[1].getMethodName().concat(": ").concat(MessageUtil.FINISH_FUNCTION));
		return new TopSecretResponse(coordinate, message);
	}

	/**
	 * Guarda la informacion del satelite en la cache del servicio de inteligencia
	 * 
	 * @param satelliteName, nombre del satelite
	 * @param request, solicitud con el mensaje en forma de arreglo y la distancia del satelite
	 * @param httpServletRequest http para obtener la ip de la solicitud de auxilio
	 * 
	 * @return confirmacion del almacenamiento del llamado de auxilio
	 */
	@Override
	public TopSecretResponse saveInformation(final String satelliteName, final TopSecretSplitRequest request, final String ipAddress) {
		
		log.debug(Thread.currentThread().getStackTrace()[1].getMethodName().concat(": ").concat(MessageUtil.START_FUNCTION));

		TopSecretRequest topSecretRequest = cache.get(ipAddress);
		topSecretRequest = Objects.isNull(topSecretRequest) ? new TopSecretRequest() : topSecretRequest;
		final Satellite satellite = Satellite.builder().distance(request.getDistance()).message(request.getMessage()).name(satelliteName).build();
		
		if (Objects.isNull(topSecretRequest.getSatellites())) {
			topSecretRequest.setSatellites(new ArrayList<Satellite>());
		} else {
			topSecretRequest.getSatellites().remove(satellite);
		}
		
		topSecretRequest.getSatellites().add(satellite);
		cache.put(ipAddress, topSecretRequest);
		
		log.debug(Thread.currentThread().getStackTrace()[1].getMethodName().concat(": ").concat(MessageUtil.FINISH_FUNCTION));
		return TopSecretResponse.builder().message(MessageUtil.MESSAGE_OK).build();
	}

	/**
	 * Obtiene el mensaje de auxilio y la ubicacion de la nave con la informacion que esta en cache
	 * 
	 * @param httpServletRequest http para obtener la ip de la solicitud de auxilio
	 * 
	 * @return retorna el mensaje de auxilio y la ubicacion
	 */
	@Override
	public TopSecretResponse getInformation(String ipAddress) throws MessageException, LocationException { // MissingInformationException,MessageNotDeterminedException
		
		log.info(Thread.currentThread().getStackTrace()[1].getMethodName().concat(": ").concat(MessageUtil.START_FUNCTION));
		
		TopSecretRequest topSecretRequest = cache.get(ipAddress);

		if (Objects.isNull(topSecretRequest) || topSecretRequest.getSatellites().size() < 3) {
			throw new MessageException(MessageUtil.MESSAGE_INSUFFICIENT_INFORMATION);
		}

		log.info(Thread.currentThread().getStackTrace()[1].getMethodName().concat(": ").concat(MessageUtil.FINISH_FUNCTION));
		return getHelpMessage(topSecretRequest);
	}


	
	/**
	 * Actualiza las posiciones de los satelites permitidos si la lista tiene infomracion
	 * 
	 * @param satelliteWrapper envoltura de los satelites necesarios para obtener el mensaje de auxilio
	 * 
	 * @return void    
	 */
	private void uploadPositions(SatelliteWrapper satelliteWrapper) {
		
		log.info(Thread.currentThread().getStackTrace()[1].getMethodName().concat(": ").concat(MessageUtil.START_FUNCTION));

		if (satelliteWrapper.getPositions()[0] == null) {
			int numberSat = Integer.parseInt(environment.getProperty("satellites.numbers"));
			double[][] pointsList = new double[numberSat][];
			String[] position;
			for (int i = 0; i < numberSat; i++) {
				if(Objects.nonNull(environment.getProperty("satellites.allowed")) && Arrays.asList(environment.getProperty("satellites.allowed").split(",")).contains(satelliteWrapper.getSatellites().get(i).getName())) {
					position = environment.getProperty("satellites." + satelliteWrapper.getSatellites().get(i).getName() + ".position").split(",");
					pointsList[i] = Arrays.stream(position).map(Double::valueOf).mapToDouble(Double::doubleValue).toArray();
				}
			}
			satelliteWrapper.setPositions(pointsList);
		}
		
		log.info(Thread.currentThread().getStackTrace()[1].getMethodName().concat(": ").concat(MessageUtil.FINISH_FUNCTION));
	}

}
