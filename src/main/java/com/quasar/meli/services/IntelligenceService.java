package com.quasar.meli.services;

import com.quasar.meli.exceptions.LocationException;
import com.quasar.meli.exceptions.MessageException;
import com.quasar.meli.pojos.TopSecretRequest;
import com.quasar.meli.pojos.TopSecretResponse;
import com.quasar.meli.pojos.TopSecretSplitRequest;

/**
 * Interface con los metodos para el servicio de inteligencia
 * 
 * @author Robinson Aguilar Ramirez, devrochy@gmail.com
 */
public interface IntelligenceService {
	
	public TopSecretResponse getHelpMessage(final TopSecretRequest request) throws MessageException, LocationException;
	public TopSecretResponse saveInformation(final String satelliteName, final TopSecretSplitRequest request, final String ipAddress);
	public TopSecretResponse getInformation(String ipAddress) throws MessageException, LocationException;

}
