package com.quasar.meli.services;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.springframework.stereotype.Service;

import com.quasar.meli.exceptions.MessageException;

/**
 * Interface con los metodos para el servicio de inteligencia
 * 
 * @author Robinson Aguilar Ramirez, devrochy@gmail.com
 */
@Service
public class MessageService {

	/**
	 * Recupera el mensaje de auxilio utilizando la informacion de los satelites
	 *  
	 * @param messageWrapper Lista de arreglos de string con la informacion de auxilio de los satelites
	 * 
	 * @return retorna el mensaje de auxilio en una cadena de texto
	 * 
	 * @throws MessageException 
	 */
	public String getMessage(List<List<String>> messageWrapper) throws MessageException {
		
		
		List<String> setWords = captureMessage(messageWrapper);
		if (!validateMessagesSize(messageWrapper, setWords.size()))
			throw new MessageException("Tamaño del mensaje incorrecto");

		removeSpace(messageWrapper, setWords.size());
		String message = completeMessage(messageWrapper);
		
		if(!validateMessagePhrases(setWords,message))
            throw new MessageException("No se puede conocer el mensaje");
		
		return message;
	}

	/**
	 * Captura el mensaje concatenando la lista de palabras posibles
	 * 
	 * @param messageWrapper
	 * @return
	 */
	public List<String> captureMessage(List<List<String>> messageWrapper) {
		List<String> listWords = new ArrayList<String>();
		for (List<String> msg : messageWrapper) {
			listWords = Stream.concat(listWords.stream(), msg.stream()).distinct().collect(Collectors.toList());
		}
		listWords.remove("");
		return listWords;
	}

	/**
	 * Valida el tamaño del mensaje 
	 * 
	 * @param messages lista con las palabras finales del mensaje
	 * @param size tamaño total que debe tener el mensaje
	 * 
	 * @return verdadero o falso si cumple con el tamaño indicado
	 */
	public boolean validateMessagesSize(List<List<String>> messages, int size) {
		for (List<String> m : messages) {
			if (m.size() < size) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Remueve los espacios de la lista de arreglos con el mensaje
	 * 
	 * @param msgList lista de arreglos con las palabras del mensaje
	 * @param gapSize tamaño de la holgura a remover
	 */
	public void removeSpace(List<List<String>> msgList, int gapSize) {
		int s = 0;
		for (int i = 0; i < msgList.size(); i++) {
			s = msgList.get(i).size();
			msgList.set(i, new ArrayList<>(msgList.get(i).subList(s - gapSize, s)));
		}
	}

	/**
	 * completa el mensaje con las palabras finales obtenidas en los filtros
	 * 
	 * @param msgList listas filtrada de arreglos de palabras con el mensaje de auxilio
	 * 
	 * @return mensaje completo de auxilio
	 */
	public String completeMessage(List<List<String>> msgList) {

		String phrase = "";
		for (List<String> m : msgList) {

			if (m.size() > 0 && !m.get(0).equals("")) {
				phrase = (m.size() == 1) ? m.get(0) : m.get(0) + " ";
				msgList.stream().forEach(s -> s.remove(0));
				return phrase + completeMessage(msgList);
			}
		}
		return "";
	}
	
	/**
	 * Valida las palabras del mensaje 
	 * 
	 * @param phrases lista de palabras
	 * @param message mensaje
	 * @return
	 */
	public boolean validateMessagePhrases(List<String> phrases, String message){
        List<String> msg = Arrays.stream(message.split(" ")).collect(Collectors.toList());
        Collections.sort(phrases);
        Collections.sort(msg);
        return Arrays.equals(phrases.toArray(), msg.toArray());
    }

}
