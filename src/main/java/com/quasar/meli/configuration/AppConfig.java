package com.quasar.meli.configuration;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

/**
 * @author Robinson Aguilar, devrochy@gmail.com
 */
@Configuration
public class AppConfig {
	/**
	   * Load Business External Configuration.
	   */
	  @Configuration
	  @PropertySource(value = "file:${data.properties.path}", encoding = "UTF-8")
	  static class DataProperties
	  {}

}
