package com.quasar.meli;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.quasar.meli.exceptions.LocationException;
import com.quasar.meli.exceptions.MessageException;
import com.quasar.meli.models.Coordinate;
import com.quasar.meli.models.Satellite;
import com.quasar.meli.models.SatelliteWrapper;
import com.quasar.meli.pojos.TopSecretRequest;
import com.quasar.meli.pojos.TopSecretResponse;
import com.quasar.meli.pojos.TopSecretSplitRequest;
import com.quasar.meli.services.IntelligenceServiceImpl;
import com.quasar.meli.utils.MessageUtil;

@SpringBootTest
@RunWith(SpringRunner.class)
@ActiveProfiles("dev")
public class IntelligenceServiceTest {

	private static final String KENOBI = "kenobi";
	private static final String SKYWALKER = "skywalker";
	private static final String SATO = "sato";
	private static final String UNKNOWN = "unknown";

	private static final float DISTANCE_TO_KENOBI = 485.69f;
	private static final float DISTANCE_TO_SKYWALKER = 266.083f;
	private static final float DISTANCE_TO_SATO = 600.5f;

	private static final String IP_ADDRESS = "10.0.0.12";

	@Autowired
	private IntelligenceServiceImpl intelligenceService;

	private List<String> messageS1;
	private List<String> messageS2;
	private List<String> messageS3;

	private TopSecretRequest topSecretRequest;
	private SatelliteWrapper satelliteList;

	@Before
	public void septup() {
		satelliteList = new SatelliteWrapper();
		topSecretRequest = new TopSecretRequest();

		messageS1 = Arrays.asList(new String[] { "Este", "es", "", "", "secreto" });
		messageS2 = Arrays.asList(new String[] { "Este", "", "un", "mensaje", "" });
		messageS3 = Arrays.asList(new String[] { "", "es", "un", "", "" });
		intelligenceService.setCache(new HashMap<String, TopSecretRequest>());
	}

	/**
	 * Identificar el mensaje de auxilio correctamente
	 */
	@Test
	public void identifierMessageTest_positionFound() throws MessageException, LocationException {

		satelliteList.getSatellites()
				.add(Satellite.builder().distance(DISTANCE_TO_KENOBI).name(KENOBI).message(messageS1).build());
		satelliteList.getSatellites()
				.add(Satellite.builder().distance(DISTANCE_TO_SKYWALKER).name(SKYWALKER).message(messageS2).build());
		satelliteList.getSatellites()
				.add(Satellite.builder().distance(DISTANCE_TO_SATO).name(SATO).message(messageS3).build());
		topSecretRequest.setSatellites(satelliteList.getSatellites());
		TopSecretResponse response = intelligenceService.getHelpMessage(topSecretRequest);
		Coordinate coordinateExpected = Coordinate.builder().positionX(-100).positionY(75.5f).build();
		assertEquals(coordinateExpected, response.getCoordinate());
		assertEquals("Este es un mensaje secreto", response.getMessage());
	}

	/**
	 * Identificar el mensaje de auxilio correctamente cuando se recibe llamado de
	 * auxilio de otras naves
	 */
	@Test
	public void identifierMessageTest_positionFoundWithExtraSatellite() throws MessageException, LocationException { // ,
																														// MessageNotDeterminedException
		satelliteList.getSatellites()
				.add(Satellite.builder().distance(DISTANCE_TO_KENOBI).name(KENOBI).message(messageS1).build());
		satelliteList.getSatellites()
				.add(Satellite.builder().distance(DISTANCE_TO_SKYWALKER).name(SKYWALKER).message(messageS2).build());
		satelliteList.getSatellites()
				.add(Satellite.builder().distance(DISTANCE_TO_SATO).name(SATO).message(messageS3).build());
		satelliteList.getSatellites().add(Satellite.builder().distance(60F).name(UNKNOWN).message(messageS3).build());
		topSecretRequest.setSatellites(satelliteList.getSatellites());
		TopSecretResponse response = intelligenceService.getHelpMessage(topSecretRequest);
		Coordinate coordinateExpected = Coordinate.builder().positionX(-100).positionY(75.5f).build();
		assertEquals(coordinateExpected, response.getCoordinate());
		assertEquals("Este es un mensaje secreto", response.getMessage());
	}

	/**
	 * Identificar la excepcion si llega la cadena de mensaje de auxilio incompleta
	 */
	@Test(expected = MessageException.class)
	public void identifierMessageTest_lessThan3Satellites() throws MessageException, LocationException {
		satelliteList.getSatellites()
				.add(Satellite.builder().distance(DISTANCE_TO_KENOBI).name(KENOBI).message(messageS1).build());
		satelliteList.getSatellites()
				.add(Satellite.builder().distance(DISTANCE_TO_SKYWALKER).name(SKYWALKER).message(messageS2).build());
		topSecretRequest.setSatellites(satelliteList.getSatellites());
		intelligenceService.getHelpMessage(topSecretRequest);
	}

	/**
	 * Identificar la expcecion si no se tiene las coordenadas de los satelites
	 */
	@Test(expected = LocationException.class)
	public void identifierMessageTest_insufficientNumberPosition() throws MessageException, LocationException {
		satelliteList.getSatellites()
				.add(Satellite.builder().distance(DISTANCE_TO_KENOBI).name(KENOBI).message(messageS1).build());
		satelliteList.getSatellites()
				.add(Satellite.builder().distance(DISTANCE_TO_SKYWALKER).name(SKYWALKER).message(messageS2).build());
		satelliteList.getSatellites().add(Satellite.builder().distance(0f).name("").message(messageS3).build());
		topSecretRequest.setSatellites(satelliteList.getSatellites());
		intelligenceService.getHelpMessage(topSecretRequest);
	}

	/**
	 * Obtener la excepcion del mensaje de auxilio si la cadena de los mensajes no
	 * es suficiente para obtener el mensaje
	 * @throws LocationException 
	 * @throws MessageException 
	 */
	@Test(expected = MessageException.class)
	public void identifierMessageTest_messageNotFoundOfSatellite2() throws MessageException, LocationException {
		messageS2 = Arrays.asList("un", "mensaje", "");
		satelliteList.getSatellites()
				.add(Satellite.builder().distance(DISTANCE_TO_KENOBI).name(KENOBI).message(messageS1).build());
		satelliteList.getSatellites()
				.add(Satellite.builder().distance(DISTANCE_TO_SKYWALKER).name(SKYWALKER).message(messageS2).build());
		satelliteList.getSatellites()
				.add(Satellite.builder().distance(DISTANCE_TO_SATO).name(SATO).message(messageS3).build());
		topSecretRequest.setSatellites(satelliteList.getSatellites());

		intelligenceService.getHelpMessage(topSecretRequest);
	}

	/**
	 * Guardar la informacion recibida por una nave kenobi en cache asociada con la
	 * ip
	 */
	@Test
	public void saveInformationTest_successForKenobi() {
		TopSecretSplitRequest request = TopSecretSplitRequest.builder().distance(DISTANCE_TO_KENOBI).message(messageS1)
				.build();
		TopSecretResponse topSecretResponse = intelligenceService.saveInformation(KENOBI, request, IP_ADDRESS);
		assertEquals(MessageUtil.MESSAGE_OK, topSecretResponse.getMessage());
	}

	/**
	 * Guardar la informacion recibida por una nave skywalker en cache asociada con
	 * la ip
	 */
	@Test
	public void saveInformationTest_successForSkywalker() {
		TopSecretSplitRequest request = TopSecretSplitRequest.builder().distance(DISTANCE_TO_SKYWALKER)
				.message(messageS2).build();
		TopSecretResponse topSecretResponse = intelligenceService.saveInformation(KENOBI, request, IP_ADDRESS);
		assertEquals(MessageUtil.MESSAGE_OK, topSecretResponse.getMessage());
	}

	/**
	 * Guardar la informacion recibida por una nave sato en cache asociada con la ip
	 */
	@Test
	public void saveInformationTest_successForSato() {
		TopSecretSplitRequest request = TopSecretSplitRequest.builder().distance(DISTANCE_TO_SATO).message(messageS3)
				.build();
		TopSecretResponse topSecretResponse = intelligenceService.saveInformation(KENOBI, request, IP_ADDRESS);
		assertEquals(MessageUtil.MESSAGE_OK, topSecretResponse.getMessage());
	}

	/**
	 * Obtener la infomracion de auxilio a partir de la cache del servicio de
	 * inteligencia
	 * 
	 * @throws MissingInformationException
	 * @throws PositionNotDeterminedException
	 * @throws MessageNotDeterminedException
	 */
	@Test
	public void getInformation_success() throws MessageException, LocationException {

		// kenobi
		TopSecretSplitRequest request = TopSecretSplitRequest.builder().distance(DISTANCE_TO_KENOBI).message(messageS1)
				.build();
		intelligenceService.saveInformation(KENOBI, request, IP_ADDRESS);

		// skywalker
		request = TopSecretSplitRequest.builder().distance(DISTANCE_TO_SKYWALKER).message(messageS2).build();
		intelligenceService.saveInformation(SKYWALKER, request, IP_ADDRESS);

		// sato
		request = TopSecretSplitRequest.builder().distance(DISTANCE_TO_SATO).message(messageS3).build();
		intelligenceService.saveInformation(SATO, request, IP_ADDRESS);

		TopSecretResponse response = intelligenceService.getInformation(IP_ADDRESS);
		Coordinate coordinateExpected = Coordinate.builder().positionX(-100).positionY(75.5f).build();
		assertEquals(coordinateExpected, response.getCoordinate());
		assertEquals("Este es un mensaje secreto", response.getMessage());
	}

	/**
	 * Obtener la informacion de auxilio a partir de la cache del servicio de
	 * inteligencia con informacion insuficiente
	 * 
	 * @throws LocationException
	 * @throws MessageException
	 */
	@Test(expected = MessageException.class)
	public void getInformation_missingSatellite() throws MessageException, LocationException {

		// kenobi
		TopSecretSplitRequest request = TopSecretSplitRequest.builder().distance(DISTANCE_TO_KENOBI).message(messageS1)
				.build();
		intelligenceService.saveInformation(KENOBI, request, IP_ADDRESS);

		// skywalker
		request = TopSecretSplitRequest.builder().distance(DISTANCE_TO_SKYWALKER).message(messageS2).build();
		intelligenceService.saveInformation(SKYWALKER, request, IP_ADDRESS);

		intelligenceService.getInformation(IP_ADDRESS);
	}

	/**
	 * Obtener la informacion de auxilio a partir de la cache del servicio de
	 * inteligencia con informacion insuficiente en el mensaje
	 * 
	 * @throws LocationException
	 * @throws MessageException
	 */
	@Test(expected = MessageException.class)
	public void getInformation_messageNotFound() throws MessageException, LocationException {

		// kenobi
		TopSecretSplitRequest request = TopSecretSplitRequest.builder().distance(DISTANCE_TO_KENOBI).message(messageS1)
				.build();
		intelligenceService.saveInformation(KENOBI, request, IP_ADDRESS);

		// skywalker
		request = TopSecretSplitRequest.builder().distance(DISTANCE_TO_SKYWALKER).message(messageS2).build();
		intelligenceService.saveInformation(SKYWALKER, request, IP_ADDRESS);

		// sato
		messageS3 = Arrays.asList("este", "", "mensaje", "");
		request = TopSecretSplitRequest.builder().distance(DISTANCE_TO_SATO).message(messageS3).build();
		intelligenceService.saveInformation(SATO, request, IP_ADDRESS);

		intelligenceService.getInformation(IP_ADDRESS);

	}

}
